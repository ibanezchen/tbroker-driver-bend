#include "odr.h"
#include "cfg.h"
#include "ts.h"
#include "broker.h"
#include "quote.h"
#include "util.h"
#include <SDL/SDL.h>
#include <stdio.h>
#include <string.h>

//      no global data escept dlopen function pointer

typedef struct {
	SDL_mutex *mut;
} priv_t;

static int login(char *_acc_pass)
{
	priv_t *priv;
	char *ID = malloc(32);
	char *passwd = malloc(32);
	char *acc_pass = malloc(strlen(_acc_pass) + 1);
	strtok(acc_pass, ",");
	strcpy(acc_pass, _acc_pass);
	tok(ID, 32);
	tok(passwd, 32);
	broker->priv = priv = malloc(sizeof(priv_t));
	priv->mut = SDL_CreateMutex();
	//TEMP:
	return -1;
}

static char *to_rsym(char *rsym, int rsym_len, char *sym)
{
	//TEMP:
	return rsym;
}

static char *to_sym(char *sym, int sym_len, char *rsym)
{
	//TEMP:
	return sym;
}

static int order(char *rsym, int vol, double _price, char *roid, int isDay)
{
	//TEMP:
	
	//call odr_new(...);
	return 0;
}

static int cancel(char *roid)
{
	//TEMP:
	return 0;
}

static double right()
{
	//TEMP:
	return 0;
}

static char *positions(char *position_buf, int sz, char sep)
{
	//TEMP:
	return "";
}

static void run()
{
	//TEMP:
	while (1) {
		SDL_Delay(1000);
	}

}

static int support(char *rsym)
{
	//TEMP:
	return 0;
}

static void _bind(char *rsym)
{
	//TEMP:
}

broker_t *bend_init(broker_t * _broker)
{
	broker = _broker;
	broker->login = login;
	broker->to_rsym = to_rsym;
	broker->to_sym = to_sym;
	broker->order = order;
	broker->cancel = cancel;
	broker->right = right;
	broker->positions = positions;
	broker->run = run;
	return broker;
}

quote_t *quote_bend_init(quote_t * quote)
{
	quote->to_rsym = to_rsym;
	quote->to_sym = to_sym;
	quote->support = support;
	quote->bind = _bind;
	return 0;
}
