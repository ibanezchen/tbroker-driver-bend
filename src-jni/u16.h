#ifndef U161225
#define U161225

#include "cfg.h"

char *u16_new(unsigned short *ws);

char *u16_getf(char *o, int n);

int u16_geti(char *s, int n);

void u16_del(char *o);

void u16_tst();

#endif
