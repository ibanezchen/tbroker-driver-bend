#ifndef BROKER0108
#define BROKER0108

typedef struct {
	///     @return 0 if success
	int (*login) (char *acc_pass);

	/// standard sym -> broker symbol 
	/// @param rsym: allocated by caller, used to save the broker's symbol
	/// @param rsym_len: length of rsym
	/// @param sym: standard symbol name, must support tx201701, mtx201702
	/// @param return rsym or 0 if not support
	char *(*to_rsym) (char *rsym, int rsym_len, char *sym);

	/// broker sym -> standard symbol 
	/// @param sym: allocated by caller, used to save the standard symbol
	/// @param sym_len: length of sym
	/// @param rsym: broker symbol name
	/// @param return rsym or 0 if not support
	char *(*to_sym) (char *sym, int sym_len, char *rsym);

	/// this is a synchronized funciton
	/// @param type: 1=Day,
	/// @return 0 if success
	int (*order) (char *rsym, int vol, double price, char *roid, int type);

	/// this is a synchronized funciton
	/// @param 0 if success, 1 if a deal has been made
	int (*cancel) (char *roid);

	double (*right) ();

	///     @param sep the seperator char
	/// @return buf in format: rsym<sep>vol<sep>rsym<sep>vol ...
	char *(*positions) (char *buf, int sz, char sep);

	void (*run) ();

	/// used to call back if the order refered by roid made deal
	void (*deal) (char *roid, double price, unsigned ts);

	void *priv;
} broker_t;

#endif
