#ifndef TS
#define TS

#include "cfg.h"

#define ts_init(_h,_m,_s)		(((_h)<<24)|((_m)<<16)|((_s)<<8))

#define ts_in_future(_ts)		((_ts) > ts_now())

extern unsigned ts_mm, ts_dd;

unsigned ts_now();

#define TS_LEN		58

int ts_to_string(unsigned ts, char *b);

#endif
