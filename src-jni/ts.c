#include "ts.h"
#include <time.h>
#include <string.h>
#include <stdio.h>

unsigned ts_mm, ts_dd;

unsigned ts_now()
{
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	ts_mm = tm->tm_mon + 1;
	ts_dd = tm->tm_mday;
	return ts_init(tm->tm_hour, tm->tm_min, tm->tm_sec);
}

int ts_to_string(unsigned ts, char *b)
{
	return sprintf(b, "%2d%2d [%2d:%2d:%2d] ",
		       ts_mm, ts_dd,
		       (ts >> 24) & 0xff, (ts >> 16) & 0xff, (ts >> 8) & 0xff);
}
