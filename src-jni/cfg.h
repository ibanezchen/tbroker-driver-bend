#ifndef CFG_DEF08
#define CFG_DEF08

#define DBG 0

void cfg_init(void);

int cfg_get_uint(char *key);

void cfglog(char *fmt, ...);

#define NOW_DAY_LEN		32

char *cfg_now(char *buf);

int starts_with(char *s, char *pre);

#define CS_I(_mut)	SDL_mutexP(_mut)

#define CS_O(_mut)	SDL_mutexV(_mut)

#define _assert(_exp)    do{\
	if(!(_exp)) {\
		printf("%s:%d\n",__FILE__,__LINE__);\
		exit(1);\
	}\
}while(0)

// CFGLOG ticking 
#define CFG_TICKING	2000

// POLL: reply lib connect
#define CFG_CAP_MON	6000

#define CFG_POS_BUF	2048

#define CFG_SINO_CANCEL_WAIT 500

#define CFG_SINO_CANCEL_TRY  6

#define CFG_MAX_ROID	512

#define CFG_SYM_MAX		32

#if !defined(snprintf)

#include <stdarg.h>
#include <stdio.h>

#define vsnprintf cfg_vsnprintf
#define snprintf cfg_snprintf
int cfg_snprintf(char *outBuf, size_t size, const char *format, ...);

int cfg_vsnprintf(char *outBuf, size_t size, const char *format, va_list ap);
#endif
#endif
