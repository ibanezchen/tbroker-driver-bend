#ifndef QUOTE0108
#define QUOTE0108

typedef struct quote {

	/// standard sym -> broker symbol 
	/// @param rsym: allocated by caller, used to save the broker's symbol
	/// @param rsym_len: length of rsym
	/// @param sym: standard symbol name, must support tx201701, mtx201702
	/// @param return rsym or 0 if not support
	char *(*to_rsym) (char *rsym, int rsym_len, char *sym);

	/// broker sym -> standard symbol 
	/// @param sym: allocated by caller, used to save the standard symbol
	/// @param sym_len: length of sym
	/// @param rsym: broker symbol name
	/// @param return rsym or 0 if not support
	char *(*to_sym) (char *sym, int sym_len, char *rsym);

	/// support quote of rsym
	int (*support) (char *rsym);

	void (*bind) (char *rsym);

	void (*tick) (char *rsym, double price, unsigned vol, unsigned ts);

} quote_t;

#endif
