#include "cfg.h"
#include "ts.h"
#include <SDL/SDL_mutex.h>
#include <SDL/SDL.h>
#include <SDL/SDL.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#ifndef _TRUNCATE
#define _TRUNCATE ((size_t)-1)
#endif
static char tsm[TS_LEN];

static SDL_mutex *cfg_mut;

void cfglog(char *fmt, ...)
{
	va_list ap;
	if (cfg_mut)
		CS_I(cfg_mut);
	printf("%s", tsm);
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	fflush(stdout);
	if (cfg_mut)
		CS_O(cfg_mut);
}

static unsigned ticking(unsigned i, void *param)
{
	unsigned ts = ts_now();
	ts_to_string(ts, tsm);
	return 2000;
}

void cfg_init(void)
{
	SDL_Init(SDL_INIT_TIMER);
	cfg_mut = SDL_CreateMutex();
	ts_to_string(ts_now(), tsm);
	SDL_AddTimer(CFG_TICKING, ticking, 0);
}

int starts_with(char *s, char *pre)
{
	for (; *pre != 0;)
		if (*s++ != *pre++)
			return 0;
	return 1;
}

char *cfg_now(char *buf)
{
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	sprintf(buf, "%4d%02d%02d",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday);
	return buf;
}

int cfg_vsnprintf(char *outBuf, size_t size, const char *format, va_list ap)
{
	int count = -1;
	if (size != 0)
		count = _vsnprintf_s(outBuf, size, _TRUNCATE, format, ap);
	if (count == -1)
		count = _vscprintf(format, ap);

	return count;
}

int cfg_snprintf(char *outBuf, size_t size, const char *format, ...)
{
	int count;
	va_list ap;

	va_start(ap, format);
	count = cfg_vsnprintf(outBuf, size, format, ap);
	va_end(ap);

	return count;
}
