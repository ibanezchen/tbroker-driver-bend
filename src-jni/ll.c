#include "ll.h"

void lle_init(lle_t * le)
{
	le->n = le->p = le;
}

char *_lle_get(lle_t * le)
{
	return (char *)le;
}

#define lle_get(l, type, mem) \
        ((type *)(_lle_get(l)-(unsigned)(&((type *)0)->mem)))

void lle_add_before(lle_t * le, lle_t * newe)
{
	lle_t *t = le->n;
	le->n = newe;
	le->p = newe;
	newe->n = t;
	newe->p = le;
}

void lle_del(lle_t * le)
{
	le->p->n = le->n;
	le->n->p = le->p;
	le->n = le->p = le;
}

#define ll_for_each(_ll, _lle)	\
	for((_lle) = ll_head(_ll); (_lle) != (void*)(_ll); (_lle) = (_lle)->n)

#define ll_for_each_mod(_ll, _lle, _tmp)	\
	for((_lle) = ll_head(_ll); (_lle) != (void*)(_ll) && ((_tmp)=(_lle)->n); (_lle) = (_tmp))

void ll_init(ll_t * l)
{
	l->n = (void *)l;
	l->p = (void *)l;
}

#define ll_empty(_l)	((_l)->n == (void*)(_l))

void ll_addh(ll_t * l, lle_t * newe)
{
	lle_t *p = l->p;

	p->n = newe;
	l->p = newe;
	newe->p = p;
	newe->n = (void *)l;
}

void ll_addt(ll_t * l, lle_t * newe)
{
	lle_t *t = l->n;

	l->n = newe;
	t->p = newe;
	newe->n = t;
	newe->p = (void *)l;
}

lle_t *ll_head(ll_t * l)
{
	return l->n;
}
