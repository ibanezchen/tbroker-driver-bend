#include "tbroker_BrokerBEnd.h"
#include "cfg.h"
#include "ll.h"
#include "ts.h"
#include "odr.h"
#include "u16.h"
#include "ts.h"
#include "util.h"
#include "quote.h"
#include "bend.h"

#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "broker.h"

#include <SDL/SDL.h>

static jclass g_class;

static JNIEnv *g_env;

static jobject g_obj;

static jmethodID eng_deal;

void bend_deal(char *roid, double price, unsigned ts)
{
	int retry, r;
	odr_t *odr;
	jbyteArray oid = 0;
	if (g_env == 0)
		return;
	odr = odr_get(roid);
	if (!odr) {
		cfglog("bend_deal: %s: %ld not found\n", roid, price);
		return;
	}
	odr->sta = ODR_DONE;
	oid = new_jbytes(g_env, odr->oid, strlen(odr->oid));
	for (retry = 0; retry < 5; retry++) {
		r = (*g_env)->CallIntMethod(g_env, g_obj, eng_deal,
					    oid, odr->vol, price, ts);
		cfglog("end_deal ret= %d\n", r);
		if (r == 0) {
			break;
		} else {
			cfglog("delay %d\n", retry);
			SDL_Delay(1000 * retry);
		}
	}
}

JNIEXPORT jint JNICALL Java_tbroker_BrokerBEnd_login
    (JNIEnv * env, jobject obj, jbyteArray _acc_pass) {
	jboolean isCopy;
	char *p;
	unsigned n;
	char *acc_pass;

	jni_init();
	broker = malloc(sizeof(broker_t));
	if (!bend_init(broker)) {
		free(broker);
		broker = 0;
	}
	broker->deal = bend_deal;
	g_env = env;
	g_obj = (*env)->NewGlobalRef(env, obj);
	g_class = (*env)->GetObjectClass(env, obj);
	cfglog("jni: login\n");
	if (g_class == 0)
		cfglog("jni: no class\n");
	eng_deal = (*env)->GetMethodID(env, g_class, "eng_deal", "([BIDJ)I");
	if (eng_deal == 0)
		cfglog("jni: no deal\n");
	p = (char *)(*env)->GetByteArrayElements(env, _acc_pass, &isCopy);
	n = strlen(p);
	acc_pass = malloc(n + 1);
	strcpy(acc_pass, p);
	(*env)->ReleaseByteArrayElements(env, _acc_pass, p, 0);
	return broker->login(acc_pass);
}

JNIEXPORT void JNICALL Java_tbroker_BrokerBEnd_run(JNIEnv * env, jobject obj)
{
	cfglog("jni: run\n");
	broker->run();
}

JNIEXPORT jbyteArray JNICALL Java_tbroker_BrokerBEnd_to_1rsym
    (JNIEnv * env, jobject obj, jbyteArray _sym) {
	jboolean isCopy;
	char rsym[CFG_SYM_MAX];
	char *sym = (char *)(*env)->GetByteArrayElements(env, _sym, &isCopy);
	cfglog("jni:to_rsym: %s\n", sym);
	broker->to_rsym(rsym, CFG_SYM_MAX, sym);
	(*env)->ReleaseByteArrayElements(env, _sym, sym, 0);
	return new_jbytes(g_env, rsym, strlen(rsym));
}

JNIEXPORT jbyteArray JNICALL Java_tbroker_BrokerBEnd_to_1sym
    (JNIEnv * env, jobject obj, jbyteArray _rsym) {
	jboolean isCopy;
	char sym[CFG_SYM_MAX];
	char *rsym = (char *)(*env)->GetByteArrayElements(env, _rsym, &isCopy);
	cfglog("jni:to_sym: %s\n", rsym);
	broker->to_sym(sym, CFG_SYM_MAX, rsym);
	(*env)->ReleaseByteArrayElements(env, _rsym, rsym, 0);
	return new_jbytes(g_env, sym, strlen(sym));
}

JNIEXPORT jdouble JNICALL Java_tbroker_BrokerBEnd_get_1right
    (JNIEnv * env, jobject obj) {
	cfglog("jni: right\n");
	return broker->right();
}

JNIEXPORT jbyteArray JNICALL Java_tbroker_BrokerBEnd_get_1positions
    (JNIEnv * env, jobject obj, jchar c) {
	jbyteArray ret;
	char *buf;
	cfglog("jni: positions %c\n", c);
	buf = malloc(CFG_POS_BUF);
	broker->positions(buf, CFG_POS_BUF, (char)c);
	ret = new_jbytes(env, buf, strlen(buf));
	free(buf);
	return ret;
}

JNIEXPORT jint JNICALL Java_tbroker_BrokerBEnd__1cancel
    (JNIEnv * env, jobject o, jbyteArray _roid) {
	jboolean isCopy;
	char *roid = (char *)(*env)->GetByteArrayElements(env, _roid, &isCopy);
	cfglog("jni: cancel %s\n", roid);
	return broker->cancel(roid);
}

JNIEXPORT jbyteArray JNICALL Java_tbroker_BrokerBEnd_order
    (JNIEnv * env, jobject o, jbyteArray _rsym, jint vol, jdouble price,
     jint isDay) {
    int ng;    
	jbyteArray ret;
	jboolean isCopy;
	char *rsym;
	char *roid = malloc(CFG_MAX_ROID);
	memset(roid, 0, CFG_MAX_ROID);
	rsym = (char *)(*env)->GetByteArrayElements(env, _rsym, &isCopy);
	cfglog("jni: order %s %d %.2f %d\n", rsym, vol, price, isDay);
	ng = broker->order(rsym, vol, price, roid, isDay);
	(*env)->ReleaseByteArrayElements(env, _rsym, rsym, 0);
	if(ng)
	    ret = 0;
	else
	    ret = new_jbytes(env, roid, strlen(roid));
	free(roid);
	return ret;
}
