#ifndef LL0320
#define LL0320

#include "cfg.h"
/// linked list entry
typedef struct lle {
	struct lle *n, *p;
} lle_t;

void lle_init(lle_t * le);

char *_lle_get(lle_t * le);

#define lle_get(l, type, mem) \
        ((type *)(_lle_get(l)-(unsigned)(&((type *)0)->mem)))

void lle_add_before(lle_t * le, lle_t * newe);

void lle_del(lle_t * le);

/// linked list head
typedef struct ll {
	lle_t *n, *p;
} ll_t;

#define ll_for_each(_ll, _lle)	\
	for((_lle) = ll_head(_ll); (_lle) != (void*)(_ll); (_lle) = (_lle)->n)

#define ll_for_each_mod(_ll, _lle, _tmp)	\
	for((_lle) = ll_head(_ll); (_lle) != (void*)(_ll) && ((_tmp)=(_lle)->n); (_lle) = (_tmp))

void ll_init(ll_t * l);

#define ll_empty(_l)	((_l)->n == (void*)(_l))

void ll_addh(ll_t * l, lle_t * newe);

void ll_addt(ll_t * l, lle_t * newe);

lle_t *ll_head(ll_t * l);

#endif
