#include "tbroker_QuoteBEnd.h"
#include "cfg.h"
#include "quote.h"
#include "util.h"
#include <string.h>
#include <stdlib.h>
#include "bend.h"

static jclass g_class;

static JNIEnv *g_env;

static jobject g_obj;

static jmethodID eng_tick;

void bend_tick(char *rsym, double price, unsigned vol, unsigned ts)
{
	char sym[CFG_SYM_MAX];
	jbyteArray jsym;
	quote->to_sym(sym, sizeof(sym), rsym);
	jsym = new_jbytes(g_env, sym, strlen(sym));
	(*g_env)->CallVoidMethod(g_env, g_obj, eng_tick, jsym, price, vol, ts);
}

JNIEXPORT void JNICALL Java_tbroker_QuoteBEnd_init(JNIEnv * env, jobject obj)
{
	jni_init();
	quote = malloc(sizeof(quote_t));
	if (!quote_bend_init(quote)) {
		free(quote);
		quote = 0;
	}
	broker = malloc(sizeof(broker_t));
	if (!bend_init(broker)) {
		free(broker);
		broker = 0;
	}
	quote->tick = bend_tick;
	g_env = env;
	g_obj = (*env)->NewGlobalRef(env, obj);
	g_class = (*env)->GetObjectClass(env, obj);
	eng_tick = (*env)->GetMethodID(env, g_class, "tick", "([BDII)V");
	if (eng_tick == 0)
		cfglog("jni: no tick\n");
}

JNIEXPORT void JNICALL Java_tbroker_QuoteBEnd_bind
    (JNIEnv * env, jobject obj, jbyteArray _rsym) {
	jboolean isCopy;
	char *rsym =
	    (char *)((*env)->GetByteArrayElements(env, _rsym, &isCopy));
	cfglog("jni: bind: %s\n", (char *)rsym);
	quote->bind(rsym);
	(*env)->ReleaseByteArrayElements(env, _rsym, rsym, 0);
}

JNIEXPORT jint JNICALL Java_tbroker_QuoteBEnd_support
    (JNIEnv * env, jobject obj, jbyteArray _rsym) {
	jboolean isCopy;
	int ret;
	char *rsym = (char *)(*env)->GetByteArrayElements(env, _rsym, &isCopy);
	ret = quote->support(rsym);
	(*env)->ReleaseByteArrayElements(env, _rsym, rsym, 0);
	return ret;
}

JNIEXPORT jbyteArray JNICALL Java_tbroker_QuoteBEnd_to_1rsym
    (JNIEnv * env, jobject obj, jbyteArray _sym) {
	jboolean isCopy;
	char rsym[CFG_SYM_MAX];
	char *sym = (char *)(*env)->GetByteArrayElements(env, _sym, &isCopy);
	cfglog("jni:to_rsym: %s\n", sym);
	broker->to_rsym(rsym, CFG_SYM_MAX, sym);
	(*env)->ReleaseByteArrayElements(env, _sym, sym, 0);
	return new_jbytes(g_env, rsym, strlen(rsym));
}
