#include "util.h"
#include <string.h>

#include <SDL/SDL.h>
#include "odr.h"
#include "cfg.h"
#include "bend.h"

void _tok(char *name, char *dst, int n)
{
	char *p = strtok(0, ",");
	if (p) {
		strncpy(dst, p, n);
		dst[n - 1] = 0;
		cfglog("%s = %s\n", name, dst);
	} else {
		cfglog("no %s\n", name);
	}
}

jbyteArray new_jbytes(JNIEnv * env, char *b, unsigned sz)
{
	jbyteArray jb = (*env)->NewByteArray(env, sz);
	(*env)->SetByteArrayRegion(env, jb, 0, sz, b);
	return jb;
}

broker_t *broker;

quote_t *quote;

static int inited;
void jni_init()
{
	int r;
	if (inited)
		return;
	r = SDL_Init(SDL_INIT_TIMER);
	atexit(SDL_Quit);
	printf("SDL_Init:%d\n", r);
	cfg_init();
	odr_init();
	inited = 1;

}
