#ifndef ODR1225
#define ODR1225

#include "ll.h"
#include "cfg.h"

typedef enum {
	ODR_NEW = 0,
	ODR_CANCEL_PEND = 1,
	ODR_CANCEL = 2,
	ODR_DONE = 3,
} odr_sta_t;

typedef struct odr {
	lle_t ll;
	char oid[CFG_MAX_ROID];	///< broker's raw order id
	int vol;
	double pri;
	unsigned ts;
	odr_sta_t sta;		///< status
	void *priv;
	void *priv2;
} odr_t;

odr_t *odr_new(char *roid, int vol, double pri);

odr_t *odr_get(char *roid);

extern ll_t odrs;;

void odr_init();

void odr_tst();

#endif
