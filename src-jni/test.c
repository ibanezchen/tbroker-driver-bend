#include "broker.h"
#include "quote.h"
#include "cfg.h"
#include "bend.h"
#include "odr.h"
#include "util.h"

#include <time.h>
#include <SDL/SDL.h>

#define REALSHIT		1
#define PRICE_DEF		8500

static char positions[1024];

static char sym[128], rsym[128];

static SDL_sem *sem;

static double pri;

static char *cur_sym(char *buf)
{
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	if (tm->tm_mon == 11) {	// december 
		sprintf(buf, "tx%4d%02d", tm->tm_year + 1 + 1900, 1);
	} else {
		sprintf(buf, "tx%4d%02d",
			tm->tm_year + 1900, tm->tm_mon + 1 + 1);
	}
	return buf;
}

void tst_tick(char *_rsym, double _pri, unsigned vol, unsigned ts)
{
	_assert(strcmp(_rsym, rsym) == 0);
	pri = _pri;
	SDL_SemPost(sem);
}

void tst_deal(char *roid, double price, unsigned ts)
{
	odr_t *odr = odr_get(roid);
	if (!odr) {
		cfglog("tst_deal: ng\n");
		return;
	}
	odr->sta = ODR_DONE;
}

void test_static(broker_t * broker, char *rsym)
{
	static char roid[CFG_MAX_ROID];
	odr_t *odr;
	cfglog("order: %s\n",
	       broker->order(rsym, 1, pri - 80, roid, 1) == 0 ? "OK" : "ng");
	cfglog("roid = %s\n", roid);
	SDL_Delay(5000);
	cfglog("cancel: %s\n", broker->cancel(roid) == 0 ? "OK" : "ng");
	odr = odr_get(roid);
	odr->sta = ODR_CANCEL;
#if REALSHIT
	cfglog("order: %s\n",
	       broker->order(rsym, 1, 0, roid, 1) == 0 ? "OK" : "ng");
	SDL_Delay(10000);
	odr = odr_get(roid);
	cfglog("deal: %s\n", odr != 0 && odr->sta == ODR_DONE ? "OK" : "ng");

	broker->positions(positions, sizeof(positions), ',');
	cfglog("positionts: %d\n", strstr(positions, rsym) != 0 ? "OK" : "ng");
	cfglog("order: %s\n",
	       broker->order(rsym, -1, 0, roid, 1) == 0 ? "OK" : "ng");
#endif
}

static int broker_run(void *p)
{
	broker->run();
	return 0;
}

#include "u16.h"

static int tst(void* p)
{	
	odr_tst();
	u16_tst();	
	cfglog("right > 50000: %s\n", (broker->right() > 50000) ? "OK" : "ng");
	cur_sym(sym);
	_assert(broker->to_rsym(rsym, sizeof(rsym), sym) != 0);
	quote = malloc(sizeof(quote_t));
	if (!quote_bend_init(quote)) {
		free(quote);
		quote = 0;
	}
	if (quote) {
		quote->tick = tst_tick;
		sem = SDL_CreateSemaphore(0);
		quote->bind(rsym);
		SDL_SemWait(sem);
		_assert(pri != 0);
	} else {
		pri = PRICE_DEF;
	}
	if (broker) {
		test_static(broker, rsym);
	}
	return 0;
}

void bend_test(char* ACCPASS)
{
	jni_init();
	broker = malloc(sizeof(broker_t));
	if (!bend_init(broker)) {
		free(broker);
		broker = 0;
	}else{
		broker->deal = tst_deal;
	}
	if(broker)
		_assert(broker->login(ACCPASS) == 0);
	SDL_CreateThread(tst, 0);
	if(broker)
		broker->run();
}
