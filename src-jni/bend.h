#ifndef ENG_bend_0108
#define ENG_bend_0108

#include "broker.h"
#include "quote.h"

#if LINUX
#define LINK_JNI
#else
#ifdef PRJ_JNI_EXPORTS
#define LINK_JNI __declspec(dllexport)
#else
#define LINK_JNI __declspec(dllimport)
#endif
#endif

///< @return 0 if not support broker
LINK_JNI broker_t *bend_init(broker_t * broker);

///< @return 0 if not support quote
LINK_JNI quote_t *quote_bend_init(quote_t * quote);

LINK_JNI void bend_test(char* ass_pass);

#endif
