#include "odr.h"
#include "ll.h"
#include "cfg.h"
#include "ts.h"
#include <SDL/SDL_mutex.h>

ll_t odrs;

static SDL_mutex *odr_mut;

odr_t *odr_new(char *roid, int vol, double pri)
{
	odr_t *o = (odr_t *) malloc(sizeof(odr_t));
	memset(o, 0, sizeof(odr_t));
	lle_init(&o->ll);
	memcpy(o->oid, roid, strlen(roid));
	o->vol = vol;
	o->pri = pri;
	o->ts = ts_now();
	o->sta = ODR_NEW;
	CS_I(odr_mut);
	ll_addh(&odrs, &o->ll);
	CS_O(odr_mut);
	return o;
}

odr_t *odr_get(char *roid)
{
	odr_t *o = 0;
	lle_t *p;
	CS_I(odr_mut);
	ll_for_each(&odrs, p) {
		odr_t *to = lle_get(p, odr_t, ll);
		if (!strcmp(to->oid, roid)) {
			o = to;
			break;
		}
	}
	CS_O(odr_mut);
	return o;
}

void odr_init()
{
	ll_init(&odrs);
	odr_mut = SDL_CreateMutex();
}

static void odr_del(odr_t * o)
{
	CS_I(odr_mut);
	lle_del(&o->ll);
	CS_O(odr_mut);
	free(o);
}

void odr_tst()
{
	odr_t *o;
	odr_init();
	_assert(ll_empty(&odrs));
	odr_new("abcd", 1, 8000);
	_assert(!ll_empty(&odrs));
	o = odr_get("abcd");
	_assert(!strcmp(o->oid, "abcd"));
	odr_del(o);
	_assert(ll_empty(&odrs));
}
