#ifndef UTIL1025
#define UTIL1025

#define tok(_s, _n)	_tok(#_s, _s, _n)
#include "cfg.h"

void _tok(char *name, char *dst, int n);

#include <jni.h>

jbyteArray new_jbytes(JNIEnv * env, char *b, unsigned sz);

#include "quote.h"
#include "broker.h"

extern broker_t *broker;

extern quote_t *quote;

void jni_init(void);

#endif
