#include "u16.h"
#include "ll.h"
#include "cfg.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *u16_new(unsigned short *ws)
{
	int l, i, sz;
	char *o, *s;
	for (l = 0; *ws++; l++) ;
	ws--;
	ws -= l;
	sz = l + 1 + sizeof(ll_t);
	o = (char *)malloc(sz);
	memset(o, 0, sz);
	ll_init((ll_t *) o);
	s = o + sizeof(lle_t);
	for (i = 0; i < l; i++)
		s[i] = 0xff & ws[i];
	return s;
}

char *u16_getf(char *s, int n)
{
	ll_t *ll = (ll_t *) (s - sizeof(ll_t));
	lle_t *lle;
	int i;
	const char *h, *t;
	char *f;
	h = s;
	if (!n)
		h--;
	for (i = 0; i < n && h; i++) {
		h = strstr(h + 1, ",");
	}
	if (!h)
		return 0;
	h++;
	t = strstr(h, ",");
	if (!t)
		t = h + strlen(h);
	lle = malloc(t - h + 1 + sizeof(lle_t));
	f = ((char *)lle) + sizeof(lle_t);
	memcpy(f, h, t - h);
	f[t - h] = 0;
	ll_addh(ll, lle);
	return f;
}

int u16_geti(char *s, int n)
{
	char *f = u16_getf(s, n);
	int i = atoi(f);
	lle_t *ll = (lle_t *) (f - sizeof(lle_t));
	lle_del(ll);
	free(ll);
	return i;
}

void u16_del(char *o)
{
	lle_t *p, *t;
	ll_t *ll = (ll_t *) (o - sizeof(ll_t));
	ll_for_each_mod(ll, p, t) {
		lle_del(p);
		free(p);
	}
	free(ll);
}

void u16_tst()
{
	short ms[] = { 'h', ',', 'e', 'e', ',', 'l', 'l', 'l', 0 };
	char *o = u16_new(ms);
	_assert(!strcmp(o, "h,ee,lll"));
	_assert(!strcmp(u16_getf(o, 0), "h"));
	_assert(!strcmp(u16_getf(o, 1), "ee"));
	_assert(!strcmp(u16_getf(o, 2), "lll"));
	u16_del(o);
}
