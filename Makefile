.PHONY:mingw	
mingw32:	
	sh/cfg-mo-mingw32.sh
mingw:	
	sh/cfg-mo-mingw.sh

VPATH+=src-jni
CONFIG=-DLINUX=1 -I$(JAVA_HOME)/include -I$(JAVA_HOME)/include/linux

%.o:%.c
	gcc -c -g $(CONFIG) -o $@ $^

lut.run:
	./lut.elf
