#!/bin/bash
name=$1
cname=`echo $name | tr A-Z a-z`
if [ "$name" == "" ]; then
	echo "\$1 = name"
	exit -1
fi
for f in `find src -name "*.java"`; do 
	nn=`echo $f|sed -e "s/BEnd/$name/g" `
	if [ "$f" == "$nn" ];then
		continue
	fi
	echo $f '=>' $nn
	mv $f $nn
	sed -e "s/BEnd/$name/g" -i $nn
done
sed -e "s/BEnd/$name/g" -i build.xml
sed -e "s/_bend/_$cname/g" -i build.xml
sed -e "s/-bend/-$cname/g" -i build.xml
sed -e "s/\/bend/\/$cname/g" -i build.xml
for f in `find src-jni -name "*.[hc]"`; do 
#	nn=`echo $f|sed -e "s/BEnd/$name/g" `
#	if [ "$f" == "$nn" ];then
#		continue
#	fi
#	echo $f '=>' $nn
#	mv $f $nn
	nn=$f
	sed -e "s/BEnd\([^.]\)/$name\1/g" -i $nn
	sed -e "s/bend_/${cname}_/g" -i $nn
done
