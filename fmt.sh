#!/bin/bash
for f in `find -name *.java`; do
	sed -e 's/^\t$//g' -i $f
	sed -e "s/[ \t]*$//g" -i $f
done
for c in src-jni/*.[hc]; do
	indent -npro -kr -i8 -ts8 -sob -l80 -ss -ncs -cp1 -il0 $c
done

