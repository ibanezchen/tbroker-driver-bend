package tbroker;

import java.util.*;

public class BrokerBEnd extends BrokerImpl implements Broker, Runnable
{
	private native int login(byte[] acc_pass);

	public native void run();

	public void login(String acc_pass)
	{
		if(login(toCStr(acc_pass)) != 0)
			throw new RuntimeException("login fail");
		new Thread(this).start();
	}

	public native byte[] to_rsym(byte[] sym);

	public String toRSym(String sym)
	{
		byte[] rsym = to_rsym(toCStr(sym));
		if(rsym == null)
			return null;
		return new String(rsym);
	}

	public native byte[] to_sym(byte[] rlsym);

	public String toSym(String rsym)
	{
		byte[] sym = to_sym(toCStr(rsym));
		if(sym == null)
			return null;
		return new String(sym);
	}

	public native double get_right();

	public native byte[] get_positions(char sep);

	public native int _cancel(byte[] roid);

	public native byte[] order(byte[] rsym_cs, int vol, double pri, int type);

}
