package tbroker;

import java.util.*;

public class QuoteBEnd extends Util implements Quote
{
	long dbase;
	QuoteTXHelper qhelper;

	QuoteBEnd()
	{
		qhelper = new QuoteTXHelper("8:44:50", "13:44:30");
		dbase = parseL(String.format("%s 00:00:00", format(new Date()))).getTime();
		init();
	}

	public void login(String acc_pass)throws Exception{}

	public native void init();

	public native void bind(byte[] rsym);

	public native int support(byte[] rsym);

	public void tick(byte[] _sym, double pri, int vol, int ts)
	{
		try{
			Tick tick = new Tick(dbase, ts>>8, vol, pri);
			String sym = new String(_sym);
			LinkedList<QuoteListener> ll = qhelper.listeners.get(sym);
			for(QuoteListener l: ll)
				l.tick(tick);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean support(String rsym)
	{
		return support(toCStr(rsym)) != 0? true:false;
	}

	public native byte[] to_rsym(byte[] sym);

	public String toRSym(String sym)
	{
		byte[] rsym = to_rsym(toCStr(sym));
		if(rsym == null)
			return null;
		return new String(rsym);
	}

	public void bind(String sym, QuoteListener l)
	{
		qhelper.bind(sym, l);
		byte[] rsym = toCStr(toRSym(sym));
		bind(rsym);
	}
}
